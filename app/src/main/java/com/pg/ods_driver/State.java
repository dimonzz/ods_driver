package com.pg.ods_driver;
import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;
import org.json.JSONObject;


public class State{
    private Context c;
    //JsonObject state;
    private JSONObject state;
    private String str;

    public State(Context c){
        this.c = c;
        reload();
    }

    public void reload(){
        str = PreferenceManager
                .getDefaultSharedPreferences(c)
                .getString("pg.ods.state","{}");
        try {
            state = new JSONObject(str);
        }catch(Exception e){
            Log.e("PG.ERROR", "Can't restore state in state constructor");
        }
    }

    public void set(String key, String value){
        try {
            state.put(key, value);
            save();
        }catch(Exception e){
            Log.e("PG.ERROR", "Can't set " + key + " = " + value + " in state");
        }
    }

    public String get(String key){
        try {
            return state.get(key).toString();
        }catch(Exception e){
            Log.e("PG.ERROR", "Can't get " + key + " from state");
            return null;
        }
    }

    public void remove(String key){
        try {
            state.remove(key);
            save();
        }catch(Exception e){
            Log.e("PG.ERROR", "Can't remove " + key + " from state");
        }
    }
    private void save(){
        PreferenceManager.getDefaultSharedPreferences(c).edit()
                .putString("pg.ods.state",state.toString()).apply();
    }

    public String getTime(){
        String str2 = PreferenceManager
                .getDefaultSharedPreferences(c)
                .getString("pg.ods.state","{}");
        try {
            JSONObject state2 = new JSONObject(str);
            return state2.get("lastSent").toString();
        }catch(Exception e){
            Log.e("PG.ERROR", "Can't get time");
            return null;
        }
    }
}
