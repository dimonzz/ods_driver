package com.pg.ods_driver.models;

/**
 * Created by kot on 01.06.17.
 */

public class AuthResult {
    public final String token;
    public final String id;
    public final String flasher_mac;
    public final String fullname;

    public AuthResult(String token, String id, String flasher_mac, String full_name){
        this.token = token;
        this.id = id;
        this.flasher_mac = flasher_mac;
        this.fullname = full_name;
    }
}
