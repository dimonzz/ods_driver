package com.pg.ods_driver.models;

/**
 * Created by kot on 01.06.17.
 */

public class TokenizedRequest {
    final String token;

    public TokenizedRequest(String token) {
        this.token = token;
    }
}
