package com.pg.ods_driver.models;

import java.util.Date;

/**
 * Created by kot on 02.06.17.
 */

public class FlasherState {

    public int uid;
    public long date;
    public boolean enabled;

    public FlasherState(int uid, boolean enabled){
        this.uid = uid;
        this.date = new Date().getTime() / 1000;
        this.enabled = enabled;
    }
}
