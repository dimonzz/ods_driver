package com.pg.ods_driver.models;

/**
 * Created by kot on 01.06.17.
 */

public class LoginRequest {
    final String username;
    final String password;

    public LoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
