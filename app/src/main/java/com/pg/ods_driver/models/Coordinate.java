package com.pg.ods_driver.models;

import android.location.Location;
import java.util.Date;

/**
 * Created by kot on 02.06.17.
 */

public class Coordinate {

    public double alt;
    public double brng;
    public double lat;
    public double lng;
    public double spd;
    public int uid;
    public long date;
    public double dist;

    public Coordinate(Location location, String uid, double dist){
        Long time = new Date().getTime() / 1000;
        this.alt = location.getAltitude();
        this.brng = location.getBearing();
        this.lat = location.getLatitude();
        this.lng = location.getLongitude();
        this.spd = location.getSpeed();
        this.date = time;
        this.uid = Integer.parseInt(uid);
        this.dist = dist;

    }
}
