package com.pg.ods_driver.models;

import java.util.ArrayList;
import java.util.List;


public class GeolocationRequestBody {
    public List<Coordinate> tracking = new ArrayList<Coordinate>();
    public List<FlasherState> state = new ArrayList<FlasherState>();
    public String token;
    public PhoneState phone_state = new PhoneState();
}
