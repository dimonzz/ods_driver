package com.pg.ods_driver.services;

import com.crashlytics.android.Crashlytics;
import com.pg.ods_driver.models.FlasherState;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.BatteryManager;
import android.os.IBinder;
import android.util.Log;

import com.pg.ods_driver.State;
import com.pg.ods_driver.models.Coordinate;
import com.pg.ods_driver.models.GeolocationRequestBody;
import com.pg.ods_driver.retrofit_services.DriverRetrofitService;
import com.pg.ods_driver.retrofit_services.ServicesManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.fabric.sdk.android.Fabric;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kot on 27.06.17.
 */

public class SenderService extends Service {
    private String TAG = "PG.V2";
    BroadcastReceiver br;
    private GeolocationRequestBody requestBody = new GeolocationRequestBody();
    private List<Integer> btStates = new ArrayList<Integer>();

    private String id;
    private String mac;
    private String token;

    private Timer senderTimer;

    public void onCreate() {
        super.onCreate();
        addCrashlytics();
        Log.e(TAG, "Sender onCreate");
    }

    private void addCrashlytics(){
        State state = new State(getApplicationContext());
        Fabric.with(this, new Crashlytics());
        //Log.e(TAG, "UID IN SENDER: " + state.get("id"));
        Crashlytics.setUserIdentifier(state.get("id"));
    }

    public void onDestroy()
    {
        Log.e(TAG, "Sender onDestroy");
        clearSenderTimer();
        clearRequestBody();
        unregisterReceiver(br);
    }

    public IBinder onBind(Intent intent){
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        Log.e(TAG, "Sender onStart");

        try{
            if(intent != null && intent.getStringExtra("token") != null){
                id = intent.getStringExtra("id");
                token = intent.getStringExtra("token");
                mac = intent.getStringExtra("mac");
            }else{
                State state = new State(getApplicationContext());
                if(state.get("token") != null) {
                    id = state.get("id");
                    token = state.get("token");
                    mac = state.get("mac");
                }
            }
            //Log.e(TAG, "Token in sender on start: " + token);
            requestBody.token = token;
        }catch(Exception e){
            Log.e(TAG, "Eror in sender: " + e.getMessage());
        }

        addBroadcastReciever();
        addSenderTimer();

        return START_STICKY;
    }

    public void addBroadcastReciever(){
        br = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                //Log.i(TAG, "Message received");
                try {
                    String type = intent.getStringExtra("messageType");
                    if (type.equals("locationUpdated")){

                        Log.e(TAG, "Sender received: " + type);

                        if(!checkTokenInRequestBody()){
                            return;
                        }

                        try {
                            Location location = new Location("gps");
                            location.setAltitude(intent.getDoubleExtra("alt", 0));
                            location.setBearing(intent.getFloatExtra("brng", 0));
                            location.setLatitude(intent.getDoubleExtra("lat", 0));
                            location.setLongitude(intent.getDoubleExtra("lng", 0));
                            location.setSpeed(intent.getFloatExtra("spd", 0));

                            Coordinate coord = new Coordinate(location, id, intent.getDoubleExtra("dist", 0));
                            requestBody.tracking.add(coord);
                            Log.e(TAG, "Location updated in sender");
                        }catch(Exception e){
                            Log.e(TAG, "Can't recieve geolocation: " + e.getMessage());
                            Log.e(TAG, "LAT: " + intent.getDoubleExtra("lat",0));
                            Log.e(TAG, "LNG: " + intent.getDoubleExtra("lng",0));
                            Log.e(TAG, Float.toString(intent.getFloatExtra("spd",0)));
                        }

                        if(btStates.size() > 0){
                            try {
                                Integer lastState = btStates.get(btStates.size() - 1);
                                FlasherState flasherState = new FlasherState(Integer.parseInt(id), lastState == 1);
                                requestBody.state.add(flasherState);
                            }catch(Exception e){
                                Log.e(TAG, "Can't add state to response body: " + e.getMessage());
                            }
                        }
                    }

                    if (type.equals("bluetoothUpdated")){
                        try{
                            Integer state = intent.getIntExtra("btState",0);
                            btStates.add(state);
                        }catch (Exception e){
                            Log.e(TAG, "Cant recieve bt state in sender: " + e.getMessage());
                        }
                    }

                    if(type.equals("userUpdated")){
                        id = intent.getStringExtra("id");
                        token = intent.getStringExtra("token");
                        mac = intent.getStringExtra("mac");
                        requestBody.token = token;
                    }

                    if(type.equals("userLoggedOut")){
                        id = null;
                        token = null;
                        mac = null;
                        clearSenderTimer();
                        clearRequestBody();
                    }

                }catch(Exception e){
                    Log.e(TAG, "Failed to receive: " + e.getMessage());
                }
            }
        };
        IntentFilter intFilt = new IntentFilter(DispatcherService.BROADCAST_ACTION);
        registerReceiver(br, intFilt);
    }

    public void clearSenderTimer(){
        if(senderTimer != null) {
            senderTimer.cancel();
        }
    }

    public boolean checkTokenInRequestBody(){
        //Log.e(TAG, "Token in geo request body: " + token);
        if(token == null){
            clearRequestBody();
            return false;
        }
        return true;
    }

    private int getBatteryLevel(){
        try {
            Intent batteryIntent = getBatteryIntent();
            int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            return Math.round(((float) level / (float) scale) * 100.0f);
        }catch(Exception e){
            Log.e(TAG, "Can't get battery level");
            return 0;
        }
    }

    private boolean isBatteryCharging(){
        try {
            Intent batteryIntent = getBatteryIntent();
            int status = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            return status == BatteryManager.BATTERY_STATUS_CHARGING;
        }catch(Exception e){
            Log.e(TAG, "Can't check if battery is charging");
            return false;
        }
    }

    private Intent getBatteryIntent(){
        return registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    public void clearRequestBody(){
        requestBody.token = null;
        requestBody.tracking.clear();
        requestBody.state.clear();
        requestBody.phone_state.battery_level = 0;
        requestBody.phone_state.charging = false;
    }

    public void addSenderTimer(){
        class SendData extends TimerTask {

            public void run() {
                try {

                    if(!checkTokenInRequestBody()){
                        return;
                    }

                    DriverRetrofitService service = ServicesManager.getDriverService();

                    List<Coordinate> coordinatesLeft = new ArrayList<Coordinate>();

                    if (requestBody.tracking.size() > 100) {
                        coordinatesLeft = requestBody.tracking.subList(99, requestBody.tracking.size() - 1);
                        requestBody.tracking = requestBody.tracking.subList(0, 99);
                    }

                    requestBody.phone_state.battery_level = getBatteryLevel();
                    requestBody.phone_state.charging = isBatteryCharging();


                    Call<ResponseBody> result = service.sendCoords(requestBody);


                    final List<Coordinate> finalCoordinatesLeft = coordinatesLeft;
                    result.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call,
                                               Response<ResponseBody> response) {

                            try {

                                Intent intent = new Intent(DispatcherService.BROADCAST_ACTION);
                                intent.putExtra("messageType", "locationSent");
                                sendBroadcast(intent);

                                if (response.code() == 200) {
                                    if (finalCoordinatesLeft.size() > 0) {
                                        requestBody.tracking = finalCoordinatesLeft;
                                    } else {
                                        requestBody.tracking.clear();
                                    }
                                    requestBody.state.clear();
                                    Log.i(TAG, "Coords sent");
                                } else {
                                    Log.e(TAG, "Coords not sent: " + response.code());
                                    Log.e(TAG, "Token: " + requestBody.token);
                                    if (response.body() != null) {
                                        Log.i(TAG, "Response body: " + response.body().string());
                                    }
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Error on response: " + e.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.e(TAG, "Error with geo request: " + t.getMessage());
                        }
                    });
                }catch(Exception e){
                    Log.e(TAG, "Error in geo task: " + e.getMessage());
                }
            }
        }

        senderTimer = new Timer();
        senderTimer.schedule(new SendData(), 0, 6000);
    }
}

