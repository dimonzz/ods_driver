package com.pg.ods_driver.services;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.pg.ods_driver.State;

import io.fabric.sdk.android.Fabric;


/**
 * Created by kot on 28.06.17.
 */

public class BluetoothService  extends Service{
    private String TAG = "PG.V2.BT";
    private BroadcastReceiver br;
    private BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
    private String mac;
    private static final boolean D = true;

    private String readUntil(String c) {
        String data = "";
        int index = buffer.indexOf(c, 0);
        if (index > -1) {
            data = buffer.substring(0, index + c.length());
            buffer.delete(0, index + c.length());
        }
        return data;
    }

    private final Handler mHandler = new Handler() {

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_READ:
                    try {
                        buffer.append((String) msg.obj);

                        String message = readUntil("\n");

                        if (message.length() > 0) {
                            Log.e(TAG, "RAW: " + message);
                            Intent intent = new Intent(DispatcherService.BROADCAST_ACTION);
                            intent.putExtra("messageType", "bluetoothUpdated");
                            String cleanedData = message.trim();
                            int delimiterIndex = cleanedData.indexOf(';');
                            cleanedData = cleanedData.substring(delimiterIndex + 1, delimiterIndex + 2);
                            Log.e(TAG, "INT: " + cleanedData);
                            intent.putExtra("btState", Integer.parseInt(cleanedData));
                            sendBroadcast(intent);
                        }


                    }catch(Exception e){
                        Log.e(TAG, "Can't recieve BT state in mHandler: " + e.getMessage());
                    }

                    break;
                case MESSAGE_READ_RAW:
                    /*if (rawDataAvailableCallback != null) {
                        byte[] bytes = (byte[]) msg.obj;
                        sendRawDataToSubscriber(bytes);
                    }*/
                    break;
                case MESSAGE_STATE_CHANGE:

                    if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothSerialService.STATE_CONNECTED:
                            Log.i(TAG, "BluetoothSerialService.STATE_CONNECTED");
                            //notifyConnectionSuccess();
                            break;
                        case BluetoothSerialService.STATE_CONNECTING:
                            Log.i(TAG, "BluetoothSerialService.STATE_CONNECTING");
                            break;
                        case BluetoothSerialService.STATE_LISTEN:
                            Log.i(TAG, "BluetoothSerialService.STATE_LISTEN");
                            break;
                        case BluetoothSerialService.STATE_NONE:
                            Log.i(TAG, "BluetoothSerialService.STATE_NONE");
                            break;
                    }
                    break;
                case MESSAGE_WRITE:
                    //  byte[] writeBuf = (byte[]) msg.obj;
                    //  String writeMessage = new String(writeBuf);
                    //  Log.i(TAG, "Wrote: " + writeMessage);
                    break;
                case MESSAGE_DEVICE_NAME:
                    Log.i(TAG, msg.getData().getString(DEVICE_NAME));
                    break;
                case MESSAGE_TOAST:
                    String message = msg.getData().getString(TOAST);
                    //notifyConnectionLost(message);
                    break;
            }
        }
    };

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSerialService bluetoothSerialService;

    StringBuffer buffer = new StringBuffer();
    private String delimiter = "\n";

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_READ_RAW = 6;
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    public IBinder onBind(Intent intent){
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId)
    {
        super.onStartCommand(intent, flags, startId);

        if(intent != null && intent.getStringExtra("mac") != null){
            mac = intent.getStringExtra("mac");
        }else{
            State state = new State(getApplicationContext());
            mac = state.get("mac");
        }

        Log.e("PG.V2", "MAC: " + mac);

        connectToDriverDevice();

        return START_STICKY;
    }

    public void onCreate(){
        addBroadcastReciever();
        addCrashlytics();
        super.onCreate();
    }

    private void addCrashlytics(){
        State state = new State(getApplicationContext());
        Fabric.with(this, new Crashlytics());
        Crashlytics.setUserIdentifier(state.get("id"));
    }

    public void onDestroy(){
        super.onDestroy();
        bluetoothSerialService.stop();
        unregisterReceiver(br);
    }

    public void addBroadcastReciever() {
        br = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Integer state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,0);
                if(state == BluetoothAdapter.STATE_ON){
                    connectToDriverDevice();
                }
                if(state == BluetoothAdapter.STATE_OFF){
                    adapter.enable();
                }
            }
        };
        IntentFilter intFilt = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(br, intFilt);
    }

    public void connectToDriverDevice(){

        if (bluetoothAdapter == null) {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }

        if (bluetoothSerialService == null) {
            bluetoothSerialService = new BluetoothSerialService(mHandler);
        }

        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(mac);

        if(!bluetoothAdapter.isEnabled()){
            bluetoothAdapter.enable();
        }

        if (device != null) {
            bluetoothSerialService.connect(device, false);
            buffer.setLength(0);
        }

    }

}
