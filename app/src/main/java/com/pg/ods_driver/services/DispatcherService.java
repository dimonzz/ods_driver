package com.pg.ods_driver.services;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.pg.ods_driver.State;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import io.fabric.sdk.android.Fabric;

/**
 * Created by kot on 27.06.17.
 */

public class DispatcherService extends Service{
    private String TAG = "PG.V2";
    public static final String BROADCAST_ACTION = "com.pg.ods_driver.message";
    Timer checkPingsTimer = new Timer();
    long lastGeolocationServicePing;
    long lastBtServicePing;
    long lastSenderPing;

    BroadcastReceiver br;

    public void onCreate() {
        super.onCreate();

        br = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Log.i(TAG + ".PING", "Message received in dispatcher: " + intent.getStringExtra("messageType"));
                try {
                    String type = intent.getStringExtra("messageType");
                    if (type.equals("locationUpdated") && intent.getDoubleExtra("lat",0) != 0 && intent.getDoubleExtra("lng",0) != 0) {
                        //Log.e(TAG, "Update geolocation ping");
                        lastGeolocationServicePing = new Date().getTime();
                    }

                    if(type.equals("bluetoothUpdated")){
                        lastBtServicePing = new Date().getTime();
                    }

                    if(type.equals("locationSent")){
                        lastSenderPing = new Date().getTime();
                    }
                }catch(Exception e){
                    Log.e(TAG, "Failed to receive ping: " + e.getMessage());
                }
            }
        };
        IntentFilter intFilt = new IntentFilter(BROADCAST_ACTION);
        registerReceiver(br, intFilt);

        class CheckPings extends TimerTask {

            public void run() {
                try {
                    Log.i(TAG + ".PING", "Check pings");
                    //Check if something was recieved from geolocation service in last 5 min
                    if (lastGeolocationServicePing != 0 && new Date().getTime() - lastGeolocationServicePing > 300000) {
                        startGeolocationService();
                    }

                    //Check if something was recieved form BT in last 5 min
                    if (lastBtServicePing != 0 && new Date().getTime() - lastBtServicePing > 300000) {
                        startBluetoothService();
                    }

                    //Check if coords was send in last 5 min
                    if (lastSenderPing != 0 && new Date().getTime() - lastSenderPing > 300000) {
                        startSenderService();
                    }

                }catch (Exception e){
                    Log.e(TAG + ".PING", "Something wrong in dispatcher on restert:" + e.getMessage());
                }
            }
        }
        checkPingsTimer.schedule(new CheckPings(), 0, 30000);
        //checkPingsTimer.schedule(new CheckPings(), 0, 5000);

        addCrashlytics();
    }

    private void addCrashlytics(){
        try {
            State state = new State(getApplicationContext());
            Fabric.with(this, new Crashlytics());
            Crashlytics.setUserIdentifier(state.get("id"));
        }catch(Exception e){
            Log.e(TAG + ".PING", "FUCK FUCK FUCK");
        }
    }

    public void startGeolocationService(){
        Log.e(TAG, "Restart geolocation service");
        Intent service = new Intent(getApplicationContext(), GeolocationService.class);
        stopService(service);
        startService(service);
    }

    public void startSenderService(){
        Log.e(TAG, "Restart sender service");
        Intent service = new Intent(getApplicationContext(), SenderService.class);
        stopService(service);
        startService(service);
    }

    public void startBluetoothService(){
        Log.e(TAG, "Restart BT service");
        Intent service = new Intent(getApplicationContext(), BluetoothService.class);
        stopService(service);
        startService(service);
        BluetoothAdapter.getDefaultAdapter().disable();
    }

    public void onDestroy()
    {
        unregisterReceiver(br);
    }

    public IBinder onBind(Intent intent){
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId)
    {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

}
