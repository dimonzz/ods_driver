package com.pg.ods_driver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.pg.ods_driver.models.AuthResult;
import com.pg.ods_driver.models.LoginRequest;
import com.pg.ods_driver.retrofit_services.LoginRetrofitService;
import com.pg.ods_driver.retrofit_services.ServicesManager;
import com.pg.ods_driver.services.BluetoothService;
import com.pg.ods_driver.services.DispatcherService;
import com.pg.ods_driver.services.GeolocationService;
import com.pg.ods_driver.services.SenderService;

import io.fabric.sdk.android.Fabric;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private BroadcastReceiver br;
    public String TAG = "PG.V2";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        //throw new Exception("Some more expetrions");

        setContentView(R.layout.activity_login);

        State state = new State(getApplicationContext());
        if(state.get("token") != null){
            onSuccessLogin(state.get("token"), state.get("id"), state.get("mac"), this);
        }
    }

    protected void onDestroy(){
        Log.e(TAG, "Login activity destroyed");
        super.onDestroy();
    }

    private void startServices(String id, String token, String mac){
        Intent serviceIntent = new Intent(this, GeolocationService.class);
        startService(serviceIntent);

        serviceIntent = new Intent(this, DispatcherService.class);
        startService(serviceIntent);

        //start sender service
        serviceIntent = new Intent(this, SenderService.class);
        serviceIntent.putExtra("token", token);
        serviceIntent.putExtra("id", id);
        serviceIntent.putExtra("mac", mac);
        startService(serviceIntent);

        //start bt service
        serviceIntent = new Intent(this, BluetoothService.class);
        serviceIntent.putExtra("token", token);
        serviceIntent.putExtra("id", id);
        serviceIntent.putExtra("mac", mac);
        startService(serviceIntent);

    }


    private void onSuccessLogin(String token, String id, String mac, final LoginActivity activity){

        final State state = new State(getApplicationContext());
        state.set("token",token);
        state.set("id",id);
        state.set("mac",mac);

        Crashlytics.setUserIdentifier(id);


        startServices(id, token, mac);

        broadcastUserData(id, token, mac);

        //Show driver page
        Intent intent = new Intent(activity, DriverActivity.class);
        startActivity(intent);
    }

    public void broadcastUserData(String id, String token, String mac){
        Intent intent = new Intent(DispatcherService.BROADCAST_ACTION);
        intent.putExtra("messageType", "userUpdated");
        //Log.d(TAG, "Token will be broadcasted: " + token);
        intent.putExtra("id", id);
        intent.putExtra("token", token);
        intent.putExtra("mac", mac);
        sendBroadcast(intent);
    }

    public void onLoginClick(View view){
        //;
        try {
            EditText loginInput = (EditText) findViewById(R.id.loginText);
            EditText passwordInput = (EditText) findViewById(R.id.passwordText);
            if (loginInput.getText().length() == 0 || passwordInput.getText().length() == 0) {
                showToast("Заповніть усі поля!");
                return;
            }

            LoginRetrofitService service = ServicesManager.getLoginService();
            String sha256hex = bin2hex(getHash(passwordInput.getText().toString())).toLowerCase();
            LoginRequest request = new LoginRequest(loginInput.getText().toString(), sha256hex);
            Call<AuthResult> result = service.login(request);

            final LoginActivity activity = this;

            result.enqueue(new Callback<AuthResult>() {
                @Override
                public void onResponse(Call<AuthResult> call,
                                       Response<AuthResult> response) {
                    try {
                        if(response.code() == 200){
                            Log.d(TAG,"Login success");
                            onSuccessLogin(response.body().token, response.body().id, response.body().flasher_mac, activity);
                        }else{
                            showToast("Не вдалося війти. Перевірте Ваші дані");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<AuthResult> call, Throwable t) {
                    Log.d(TAG, "Error with login request: " + t.getMessage());
                    t.printStackTrace();
                }
            });
        }catch(Exception e){
            Log.i(TAG,"Exception: " + e.getMessage());
        }
    }


    public byte[] getHash(String password) {
        MessageDigest digest=null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        digest.reset();
        return digest.digest(password.getBytes());
    }
    static String bin2hex(byte[] data) {
        return String.format("%0" + (data.length*2) + "X", new BigInteger(1, data));
    }

    protected void showToast(String message){
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
