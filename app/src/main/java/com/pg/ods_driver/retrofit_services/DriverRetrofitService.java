package com.pg.ods_driver.retrofit_services;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import com.pg.ods_driver.models.*;

/**
 * Created by kot on 01.06.17.
 */

public interface DriverRetrofitService {
    @POST("/apk/coords/")
    @Headers({
            "Accept: application/json, text/plain, */*",
            "User-Agent: Retrofit-Sample-App"
    })
    Call<ResponseBody> sendCoords(@Body GeolocationRequestBody body);


    @POST("/apk/event/")
    @Headers({
            "Accept: application/json, text/plain, */*",
            "User-Agent: Retrofit-Sample-App"
    })
    Call<DriverEvent> loadEvent(@Body TokenizedRequest body);

    @POST("/apk/reject-confirm/")
    @Headers({
            "Accept: application/json, text/plain, */*",
            "User-Agent: Retrofit-Sample-App"
    })
    Call<ResponseBody> confirmRejectReceived(@Body TokenizedRequest body);

    @POST("/apk/route_ok/")
    @Headers({
            "Accept: application/json, text/plain, */*",
            "User-Agent: Retrofit-Sample-App"
    })
    Call<ResponseBody> confirmEventReceived(@Body TokenizedRequest body);

}