package com.pg.ods_driver.retrofit_services;

import com.pg.ods_driver.models.AuthResult;
import com.pg.ods_driver.models.LoginRequest;
import com.pg.ods_driver.models.TokenizedRequest;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by kot on 01.06.17.
 */

public interface LoginRetrofitService {
    @POST("/apk/login/")
    @Headers({
            "Accept: application/json",
            "User-Agent: Retrofit-Sample-App"
    })
    Call<AuthResult> login(@Body LoginRequest body);

    @POST("/apk/logout/")
    @Headers({
            "Accept: application/json",
            "User-Agent: Retrofit-Sample-App"
    })
    Call<ResponseBody> logout(@Body TokenizedRequest body);
}
