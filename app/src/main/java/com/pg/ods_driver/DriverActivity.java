package com.pg.ods_driver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.pg.ods_driver.models.AuthResult;
import com.pg.ods_driver.models.DriverEvent;
import com.pg.ods_driver.models.TokenizedRequest;
import com.pg.ods_driver.retrofit_services.DriverRetrofitService;
import com.pg.ods_driver.retrofit_services.LoginRetrofitService;
import com.pg.ods_driver.retrofit_services.ServicesManager;
import com.pg.ods_driver.services.BluetoothService;
import com.pg.ods_driver.services.DispatcherService;
import com.pg.ods_driver.services.GeolocationService;
import com.pg.ods_driver.services.SenderService;

import java.util.Timer;
import java.util.TimerTask;

import io.fabric.sdk.android.Fabric;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverActivity extends AppCompatActivity {

    String TAG = "PG.V2";
    protected Timer checkForNewEvents;
    protected float eventLat;
    protected float eventLng;
    protected String token;

    public class CheckForEvents extends TimerTask {

        public void run() {
            //make call to http
            //if event found - call activiy method
            DriverRetrofitService service = ServicesManager.getDriverService();
            TokenizedRequest request = new TokenizedRequest(token);
            Call<DriverEvent> result = service.loadEvent(request);

            result.enqueue(new Callback<DriverEvent>() {
                @Override
                public void onResponse(Call<DriverEvent> call,
                                       Response<DriverEvent> response) {
                    try {
                        if(response.code() == 200){
                            eventFound(response.body());
                        }else{
                            eventNotFound();
                        }
                    } catch (Exception e) {
                        Log.e(TAG,"Error when received event: " + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<DriverEvent> call, Throwable t) {
                    Log.e(TAG,"Error whith event request: " + t.getMessage());
                }
            });

        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public void eventFound(final DriverEvent event){

        if(event.event == null){
            //nothing was changed in event
            return;
        }

        if(event.event.equals("rejected")){

            onEventRejected(event);

        }else {

            onEventRecieved(event);

        }
    }

    public void onEventRejected(final DriverEvent event){
        DriverRetrofitService service =  ServicesManager.getDriverService();
        Call<ResponseBody> call = service.confirmRejectReceived(new TokenizedRequest(token));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {
                try {
                    if(response.code() == 200){
                        showSpinner();
                        showNotification("Виклик скасовано", "");
                    }
                } catch (Exception e) {
                    Log.e(TAG,e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t){

            }
        });
    }

    public void onEventRecieved(final DriverEvent event){
        DriverRetrofitService service =  ServicesManager.getDriverService();
        Call<ResponseBody> call = service.confirmEventReceived(new TokenizedRequest(token));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {
                try {
                    if(response.code() == 200){
                        if(event.event == null){
                            //Картка не змінилася
                            return;
                        }

                        String address = event.route.address;
                        String notificationMessage = "";

                        switch (event.event){
                            case "update" :
                                notificationMessage = "Виклик оновлено";
                                break;
                            case "create" :
                                notificationMessage = "Новий виклик";
                                break;
                        }

                        Log.e("ADDRESS.VALID", "Received: " + (event.route.address_valid ? "True" : "False"));

                        showNotification(notificationMessage, address);

                        eventLat = event.route.latitude;
                        eventLng = event.route.longitude;

                        State state = new State(getApplicationContext());
                        state.set("lat",Float.toString(eventLat));
                        state.set("lng",Float.toString(eventLng));
                        state.set("address",address);
                        state.set("address_valid", Boolean.toString(event.route.address_valid));

                        updateUI(address, event.route.address_valid);

                    }
                } catch (Exception e) {
                    Log.e(TAG,e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t){

            }
        });
    }

    public void updateUI(String address, boolean address_valid){
        setEventAddress(address);
        toggleAddressValidText(address_valid);
        hideSpinner();
    }

    public void setEventAddress(String address){
        TextView el = (TextView)findViewById(R.id.addressText);
        el.setText(address);
    }

    public void showNotification(String title, String message){

        Intent intent = new Intent(this, DriverActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_healing_black_24dp)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentTitle(title)
                        .setContentText(message)
                        .setContentIntent(pIntent);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);


        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;



        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();
        mNotificationManager.notify((int)(System.currentTimeMillis() % 1000), notification);
    }

    public void toggleAddressValidText(boolean valid){
        Log.e("ADDRESS.VALID","Valid: " + (valid ? "yes" : "no"));
        View el = findViewById(R.id.addressValidText);
        int visibility = !valid ? View.VISIBLE : View.GONE;
        el.setVisibility(visibility);
    }

    public void hideSpinner(){
        View el = findViewById(R.id.progressBar);
        el.setVisibility(View.GONE);
        el = findViewById(R.id.textView);
        el.setVisibility(View.GONE);
        el = findViewById(R.id.button2);
        el.setVisibility(View.GONE);
        el = findViewById(R.id.showRouteOnMapBtn);
        el.setVisibility(View.VISIBLE);
        el = findViewById(R.id.addressText);
        el.setVisibility(View.VISIBLE);
    }

    public void showSpinner(){
        View el = findViewById(R.id.progressBar);
        el.setVisibility(View.VISIBLE);
        el = findViewById(R.id.textView);
        el.setVisibility(View.VISIBLE);
        el = findViewById(R.id.button2);
        el.setVisibility(View.VISIBLE);
        el = findViewById(R.id.showRouteOnMapBtn);
        el.setVisibility(View.GONE);
        el = findViewById(R.id.addressText);
        el.setVisibility(View.GONE);

        el = findViewById(R.id.addressValidText);
        el.setVisibility(View.GONE);
    }

    public void eventNotFound(){
        this.showSpinner();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);
        addCrashlytics();
        State state = new State(getApplicationContext());
        token = state.get("token");

        Timer timer = new Timer();
        timer.schedule(new CheckForEvents(), 0, 5000);
        this.checkForNewEvents = timer;

        if(state.get("lat") != null){
            eventLat = Float.parseFloat(state.get("lat"));
            eventLng = Float.parseFloat(state.get("lng"));
            boolean addressValid = Boolean.parseBoolean(state.get("address_valid"));
            updateUI(state.get("address"), addressValid);
        }
    }

    private void addCrashlytics(){
        State state = new State(getApplicationContext());
        Fabric.with(this, new Crashlytics());
        Crashlytics.setUserIdentifier(state.get("id"));
    }

    public void logoutBtnClicked(View view){
        logout();
    }

    private void logout(){
        State state = new State(getApplicationContext());
        LoginRetrofitService service = ServicesManager.getLoginService();
        TokenizedRequest request = new TokenizedRequest(state.get("token"));
        Call<ResponseBody> response = service.logout(request);

        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {
                try {
                    if(response.code() == 200){
                        onSuccessLogout();
                    }else{
                        showToast("Не вдалося здійснити вихід");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "Error with logout request: " + t.getMessage());
                t.printStackTrace();
            }
        });
    }

    private void onSuccessLogout(){
        checkForNewEvents.cancel();
        //stop sender and BT services
        stopService(new Intent(this, SenderService.class));
        stopService(new Intent(this, BluetoothService.class));
        stopService(new Intent(this, DispatcherService.class));
        stopService(new Intent(this, GeolocationService.class));
        //notify about logout
        Intent intent = new Intent(DispatcherService.BROADCAST_ACTION);
        intent.putExtra("messageType", "userLoggedOut");
        sendBroadcast(intent);
        //clear state
        State state = new State(getApplicationContext());
        state.remove("id");
        state.remove("token");
        state.remove("mac");
        //change activity
        Intent loginPageIntent = new Intent(this, LoginActivity.class);
        startActivity(loginPageIntent);
    }

    private void showToast(String message){
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void showRouteOnMap(View view){
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + eventLat + "," + eventLng);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }


}
